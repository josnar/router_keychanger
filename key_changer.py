import abc

class KeyChanger(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.ip = None
        self.port = None
        self.admin_username = None
        self.admin_key = None

    @abc.abstractmethod
    def change_key(self, new_key):
        """
        :return: boolean indicating if the operation succeed
        """
        return
