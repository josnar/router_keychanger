import spynner
import pyquery

from key_changer import KeyChanger
from wait import wait_for_webpage


class LiveBox21(KeyChanger):
    def __init__(self):
        super(LiveBox21, self).__init__()
        self.browser = spynner.Browser(debug_level=spynner.DEBUG)
        self.browser.set_html_parser(pyquery.PyQuery)

    def change_key(self, new_key):
        self.__login()
        self.__post_new_key(new_key)
        self.__logout()

    def __login(self):
        self.browser.load('http://' + self.ip,
                          wait_callback=lambda _: wait_for_webpage(self.browser))

        self.browser.wk_fill('input[name=user]', self.admin_username)
        self.browser.wk_fill('input[name=pws]', self.admin_key)
        self.browser.wk_click('a[class="button60l"]', wait_load=True)

    def __post_new_key(self, new_key):
        self.browser.load('http://' + self.ip + "/quicksetting.stm/0/1",
                          wait_callback=lambda _: wait_for_webpage(self.browser))
        self.browser.wk_fill('input[name=sharedkey]', new_key)
        self.browser.wk_click('a[href="javascript:document.tF0.submit()"]', wait_load=True)

    def __logout(self):
        self.browser.load('http://' + self.ip + '/cgi-bin/logout.exe')
        self.browser.close()


if __name__ == '__main__':
    import getpass
    import sys
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("--username", dest="username",
                      help="Username for logging in the router")
    parser.add_option("--ip", dest="ip",
                      help="Router ip address")
    parser.add_option("--key", dest="key",
                      help="Router ip address")
    (options, args) = parser.parse_args()

    if (not options.username or
        not options.ip or
        not options.key):
        parser.print_help()
        sys.exit()

    password = getpass.getpass("Type the password for %s: " % options.username)

    changer = LiveBox21()
    changer.ip = options.ip
    changer.admin_username = options.username
    changer.admin_key = password
    changer.change_key(options.key)
