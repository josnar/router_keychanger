from key_changer import KeyChanger
from livebox21 import LiveBox21


def get_router_classes():
    return KeyChanger.__subclasses__()

class RouterFactory(object):
    @staticmethod
    def getRouter(router_name):
        return globals()[router_name]()

# if __name__ == '__main__':
#     print get_router_classes()
#     print RouterFactory.getRouter("LiveBox21")
